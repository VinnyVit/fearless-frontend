window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';


    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        const selectTag = document.getElementById('id');

        for (let location of data.locations) {
            const option = document.createElement('option')
            option.value = location.id
            option.innerHTML = location.name


            selectTag.appendChild(option)


        }


        // For each state in the states property of the data

        // Create an 'option' element

        // Set the '.value' property of the option element to the
        // state's abbreviation

        // Set the '.innerHTML' property of the option element to
        // the state's name

        // Append the option element as a child of the select tag
    }
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
      console.log(json)
      const conferenceUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newConference = await response.json();
        console.log(newConference);
      }



    });


});
