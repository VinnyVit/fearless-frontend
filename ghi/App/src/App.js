import logo from './logo.svg';
import './App.css';
import Nav from './Nav';
import AttendeesList from './AttendeeList';
import LocationForm from './LocationForm';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
    <Nav />
    <div className="container">
      <Routes>
      <Route index element={<MainPage />} />
        <Route path="locations">
          <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="conferences">
          <Route path="new" element={<ConferenceForm/>} />
        </Route>
        <Route path="attendees" element={<AttendeesList/>}>
        </Route>
        <Route path="attendees/new" element={<AttendConferenceForm/>}/>
        <Route path="presentation/new" element={<PresentationForm/>}>

        </Route>


      </Routes>
    </div>
  </BrowserRouter>
  );
}
export default App;
