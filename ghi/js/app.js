

function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
      <div class="card">

        <div class="card-body">
          <img src="${pictureUrl}" class="card-img-top">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            ${starts} ${ends}
        </div>
      </div>
    `;
  }

function returnError(errorMessage){
    return `

    <div class="alert alert-primary" role="alert">
    ${errorMessage}
    </div>


    `;
}
// function alert() {
//     return `

//     <div id="liveAlertPlaceholder"></div>
// <button type="button" class="btn btn-primary" id="liveAlertBtn">Show live alert</button>


//     `;
// }



window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // const errorMessage = 'Invalid response, check your url or server connection.'
        // const html = returnError(errorMessage)
        // const err = document.querySelectorAll('.alert')
        // err.innerHTML += html
        //alert('Invalid response, check your url or server connection.')
        const warning = document.querySelector('.warning');
        warning.innerHTML += "An error occurred with your url or network connection"
        // Figure out what to do when the response is bad
    } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const starts = (details.conference.starts).slice(0, 10);
            const ends = (details.conference.ends).slice(0, 10);
            const location = details.conference.location.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const html = createCard(name, description, pictureUrl, starts, ends, location);
            const column = document.querySelector('.col');
            column.innerHTML += html;
          }
        }

      }
    } catch (e) {
        var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
        var alertTrigger = document.getElementById('liveAlertBtn')

        function alert(message, type) {
          var wrapper = document.createElement('div')
          wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

          alertPlaceholder.append(wrapper)
        }

        if (alertTrigger) {
          alertTrigger.addEventListener('click', function () {
            alert('There was an error, you figure it out.', 'success')
          })
        }
      // Figure out what to do if an error is raised
    }

  });
