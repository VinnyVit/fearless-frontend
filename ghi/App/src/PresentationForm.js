import { useState, useEffect } from "react";

export default function PresentationForm() {
    const [presenter_name, setPresentName] = useState('');
    const [presenter_email, setPresentEmail] = useState('');
    const [company_name, setCompName] = useState('');

    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');

    const [conferences, setConferences] = useState([]);
    const [conference, setConference] = useState('')
    const [conference_id, setConfId] = useState('')

    const handleChangePresentName = (event) => {
        const value = event.target.value;
        setPresentName(value);
      }

    const handleChangePresentEmail = (event) => {
        const value = event.target.value;
        setPresentEmail(value);
      }

    const handleChangeCompName = (event) => {
        const value = event.target.value;
        setCompName(value);
      }

    const handleChangeTitle = (event) => {
        const value = event.target.value;
        setTitle(value);
      }



    const handleChangeSynopsis= (event) => {
        const value = event.target.value;
        setSynopsis(value);
      }
    const handleChangeConference = (event) =>{
        const value = event.target.value
        setConference(value);
    }
    const fetchData = async () => {
      const url = 'http://localhost:8000/api/conferences/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences);
        setConfId(data.conferences.conference_id)

      }
    }

    useEffect(() => {
      fetchData();
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.presenter_name = presenter_name;
        data.presenter_email = presenter_email;

        data.company_name = company_name;
        data.title = title;
        data.synopsis = synopsis;

        data.conference_id = conference_id;


        const presentationUrl = `http://localhost:8000${conference}presentations/`;
        const fetchOptions = {
          method: 'post',
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const presentationResponse = await fetch(presentationUrl, fetchOptions);
        console.log(presentationResponse);
        if (presentationResponse.ok) {
          setPresentName('');
          setPresentEmail('');
          setCompName('');
          setTitle('');
          setSynopsis('');
          setConference([]);
        }
      }

return (

  <div className="container">
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new presentation</h1>
          <form onSubmit={handleSubmit} id="create-presentation-form">
            <div className="form-floating mb-3">
              <input onChange={handleChangePresentName} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
              <label htmlFor="presenter_name">Presenter name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangePresentEmail} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
              <label htmlFor="presenter_email">Presenter email</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeCompName} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
              <label htmlFor="company_name">Company name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeTitle} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
              <label htmlFor="title">Title</label>
            </div>
            <div className="mb-3">
              <label htmlFor="synopsis">Synopsis</label>
              <textarea onChange={handleChangeSynopsis} className="form-control" id="synopsis" rows="3" name="synopsis" ></textarea>
            </div>
            <div className="mb-3">
              <select onChange={handleChangeConference} required name="conference" id="conference" className="form-select">
                <option value="">Choose a conference</option>
                {conferences.map(conference => {
                      return (
                        <option key={conference.href} value={conference.href}>{conference.name}</option>
                      )
                    })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  );
}
